Install Typescript
`npm install -g typescript`

Compile the project from the the specific task folder. This should generate task.js
`tsc`

Get the packaging tool (TFS Cross Platform Command Line Interface (tfx-cli))
`npm i -g tfx-cli`

Build to a .vsix file to be uploaded to the VS Marketplace
`tfx extension create --rev-version --manifest-globs vss-extension.json`

adding `--rev-version` will rev the version specified in vss-extension.json. Version needs to be incremented to upload to VS Marketplace