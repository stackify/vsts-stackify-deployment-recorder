
import path = require('path');
import tl = require('vsts-task-lib/task');
import trm = require('vsts-task-lib/toolrunner');
import request = require('request');

import { ToolRunner } from 'vsts-task-lib/toolrunner';

async function run() {

    tl.setResourcePath(path.join(__dirname, 'task.json'));

    var apiKey: string = tl.getInput('stackifyApiKey', true);
    tl.setTaskVariable('stackifyApiKey', apiKey);

    var app: string = tl.getInput('stackifyAppName', true);
    tl.setTaskVariable('stackifyAppName', app);

    var env: string = tl.getInput('environment', true);
    tl.setTaskVariable('environment', env);

    var version: string = tl.getInput('version', true);
    tl.setTaskVariable('version', version);

    var action: string = tl.getInput('action', true);
    tl.setTaskVariable('action', action);
    
    var name: string = tl.getInput('name', false);
    tl.setTaskVariable('name', name);

    var branch: string = tl.getInput('branch', false);
    tl.setTaskVariable('branch', branch);

    var commit: string = tl.getInput('commit', false);
    tl.setTaskVariable('commit', commit);

    var uri: string = tl.getInput('uri', false);
    tl.setTaskVariable('uri', uri);
    
    console.log("API Key: " + apiKey);
    console.log("App: " + app);
    console.log("Version: " + version);
    console.log("Environment: " + env);
    console.log("Deployment Action: " + action);
    console.log("Deployment Name: " + name);
    console.log("Branch: " + branch);
    console.log("Commit: " + commit);
    console.log("URI: " + uri);

    console.log("Starting POST request...");

    var options = {
        method: 'POST',
        url: 'https://api.stackify.net/api/v1/deployments/' + action,
        headers: { 
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: 'ApiKey ' + apiKey 
        },
      form:{ 
         Version: version,
         AppName: app,
         EnvironmentName: env,
         Name: name,
         Branch: branch,
         Commit: commit,
         Uri: uri,
         DeploySystem: "VSTS"
        }
    };
    
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(response.statusCode + ' : ' + response.statusMessage);
      console.log(body);
    });
    
    console.log("Stackify Retrace Deployment Recorder task complete");
}
run();