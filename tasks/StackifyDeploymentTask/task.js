"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const tl = require("vsts-task-lib/task");
const request = require("request");
function run() {
    return __awaiter(this, void 0, void 0, function* () {
        tl.setResourcePath(path.join(__dirname, 'task.json'));
        var apiKey = tl.getInput('stackifyApiKey', true);
        tl.setTaskVariable('stackifyApiKey', apiKey);
        var app = tl.getInput('stackifyAppName', true);
        tl.setTaskVariable('stackifyAppName', app);
        var env = tl.getInput('environment', true);
        tl.setTaskVariable('environment', env);
        var version = tl.getInput('version', true);
        tl.setTaskVariable('version', version);
        var action = tl.getInput('action', true);
        tl.setTaskVariable('action', action);
        var name = tl.getInput('name', false);
        tl.setTaskVariable('name', name);
        var branch = tl.getInput('branch', false);
        tl.setTaskVariable('branch', branch);
        var commit = tl.getInput('commit', false);
        tl.setTaskVariable('commit', commit);
        var uri = tl.getInput('uri', false);
        tl.setTaskVariable('uri', uri);
        console.log("API Key: " + apiKey);
        console.log("App: " + app);
        console.log("Version: " + version);
        console.log("Environment: " + env);
        console.log("Deployment Action: " + action);
        console.log("Deployment Name: " + name);
        console.log("Branch: " + branch);
        console.log("Commit: " + commit);
        console.log("URI: " + uri);
        console.log("Starting POST request...");
        var options = {
            method: 'POST',
            url: 'https://api.stackify.net/api/v1/deployments/' + action,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'ApiKey ' + apiKey
            },
            form: {
                Version: version,
                AppName: app,
                EnvironmentName: env,
                Name: name,
                Branch: branch,
                Commit: commit,
                Uri: uri
            }
        };
        request(options, function (error, response, body) {
            if (error)
                throw new Error(error);
            console.log(response.statusCode + ' : ' + response.statusMessage);
            console.log(body);
        });
        console.log("Stackify Retrace Deployment Recorder task complete");
    });
}
run();
//# sourceMappingURL=task.js.map